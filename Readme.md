# Openstack repo

     https://mirror.nsc.liu.se/CentOS/7.9.2009/cloud/x86_64/openstack-train/


# Considerations

This is a cluster formed among 3 hosts named - openstack06.infra.ts, openstack07.infra.ts, and openstack08.infra.ts.

	openstack06.infra.ts - 10.91.4.14
	openstack07.infra.ts - 10.91.4.15
	openstack08.infra.ts - 10.91.4.16


Each of the three hosts are All-in-One setups.
For Block storage service (Cinder), only /dev/sda4 partition is used in openstack08.infra.ts.

Internally, the nodes are also referred to as controller(s), namely controller06, controller07, and controller08, since
they have management services running.

# How to read the installation steps.

Every new task starts with a hyphen ( - ) such as,

	   - A_task_description

		Task 1
		Task 2

A task description as shown above means it is to be performed on all of the nodes.


	   - A_task_description

	# openstack06 #

		Task 1
		Task 2

	# openstack07 #

		Task 1
		Task 2

A task description as shown above specifies on which specific nodes the task has to be performed.






# Add a compute node

## Services to be added to the node

        openstack-nova-compute
        openstack-neutron openstack-neutron-ml2 openstack-neutron-linuxbridge ebtables openstack-neutron-linuxbridge ipset

### Services to be running in a compute node

        openstack-nova-compute.service libvirtd.service neutron-linuxbridge-agent.service neutron-dhcp-agent.service neutron-metadata-agent.service


### Do not run neutron server on the compute node

        The neutron-dhcp-agent.service neutron-metadata-agent.service services are added to the compute node to treat it as a network node as well, alongside being a compute node.
        This creates a new DHCP agent, resolving 169.254.169.254 resolution issue.




# Known Issues

## Resetting resource provider UUID(s) - Conflicting resource provider name: openstack already exists

	Ref: https://ask.openstack.org/en/question/115081/openstack-queen-instance-creation-error-no-valid-host-was-found/


## Keystone new user creation

First create a default user ( or will get error Could not find default role "user" in keystone):

	openstack role create user


## Unable to get Metadata due to dhcp agent IP not present in VM

Check: DHCP Agents running in High-Availibility configuration - https://docs.openstack.org/neutron/pike/admin/config-dhcp-ha.html  

This may happen when metadata cannot be downloaded from 169.254.169.254 despite multiple DHCP agents running.
This is because any DHCP agent IP (from the DHCP agent pool) is not being added in routes on VM creation.

By brute force, upstream DHCP server is configured to add a particular DHCP agent IP (arbitrarily selected from pool of DHCP agents running).


Content of /etc/dhcp/dhcpd.conf

        subnet 10.10.0.0 netmask 255.255.255.0 
        {
        option routers                  10.10.0.1;
        option subnet-mask              255.255.255.0;

        option domain-name              "infra.ts";
        option domain-search "infra.ts", "office.ts", "dev.ts", "seamless.internal";

        option rfc3442-classless-static-routes 24, 169, 254, 169, 10, 10, 0, 71;
        option ms-classless-static-routes 24, 169, 254, 169, 10, 10, 0, 71;


        range 10.10.0.10 10.10.0.254;

        next-server 10.10.0.10;
        filename "pxelinux.0";
        }


Legend:  
24 - Mask  
196,254,169 - Via route (Metadata agent)  
10, 10, 0, 71 - DHCP agent IP (Chosen arbitrarily from DHCP agent pool)  

More config: https://docs.openstack.org/neutron/pike/configuration/dhcp-agent.html 

#### Change can also be made in 

# Cinder

## For enabling other users/projects/tenents to create cinder volumes:

1) Change quota from dashboard.
2) Apply per-volume-gigabytes quota from cli:


                $ cinder quota-show 5d9cd1a8b0d6456ba9927ffba232303e
                +-----------------------+-------+
                | Property              | Value |
                +-----------------------+-------+
                | backup_gigabytes      | 1000  |
                | backups               | 10    |
                | gigabytes             | 200   |
                | gigabytes___DEFAULT__ | -1    |
                | gigabytes_multiattach | -1    |
                | groups                | 10    |
                | per_volume_gigabytes  | 0     | *
                | snapshots             | 0     |
                | snapshots___DEFAULT__ | -1    |
                | snapshots_multiattach | -1    |
                | volumes               | 1     |
                | volumes___DEFAULT__   | -1    |
                | volumes_multiattach   | -1    |
                +-----------------------+-------+

                $ cinder quota-update --per-volume-gigabytes 200 5d9cd1a8b0d6456ba9927ffba232303e
                +-----------------------+-------+
                | Property              | Value |
                +-----------------------+-------+
                | backup_gigabytes      | 1000  |
                | backups               | 10    |
                | gigabytes             | 200   |
                | gigabytes___DEFAULT__ | -1    |
                | gigabytes_multiattach | -1    |
                | groups                | 10    |
                | per_volume_gigabytes  | 200   | *
                | snapshots             | 0     |
                | snapshots___DEFAULT__ | -1    |
                | snapshots_multiattach | -1    |
                | volumes               | 1     |
                | volumes___DEFAULT__   | -1    |
                | volumes_multiattach   | -1    |
                +-----------------------+-------+


# Enable VM Resize/Migrate (requires ssh nova's ability among nodes)

	https://docs.openstack.org/nova/pike/admin/ssh-configuration.html

## In-case of migration error

	https://platform9.com/kb/openstack/ostackhost-fails-to-resize-create-and-migrate-instance-with-err




## Other Refs:

	https://www.server-world.info/en/note?os=CentOS_7&p=openstack_train2&f=1


