#!/bin/bash
# Author: Snehadeep B.

# Hosts that have Openstack components installed for Controllers, Computes, Blocks, etc.
HOSTS='10.91.4.14 10.91.4.15 10.91.4.16'
# Hosts that have peripheral services - HTTPD, HAPROXY, RABBITMQ and KEEPALIVED. These hosts are mainly Controller types.
PERIPHERAL_SERVICE_HOSTS='10.91.4.14 10.91.4.15 10.91.4.16'

#VIP for the cluster
controller="10.91.4.255"

mysql_host="10.91.4.14"

# LIST OF ALL SERVICES
#"keepalived haproxy httpd libvirtd rabbitmq-server neutron-dhcp-agent neutron-linuxbridge-agent neutron-metadata-agent neutron-server openstack-glance-api openstack-glance-registry openstack-heat-api-cfn openstack-heat-api openstack-heat-engine openstack-nova-api openstack-nova-compute openstack-nova-conductor openstack-nova-consoleauth openstack-nova-novncproxy openstack-nova-scheduler openstack-cinder-api openstack-cinder-scheduler openstack-cinder-volume"

MAIL_GROUP="mail.id@mailer.com,mail.id2@mailer.com"

function post_to_slack () 
{
  # format message as a code block ```${msg}```
  SLACK_MESSAGE="\`\`\`$1\`\`\`"
  SLACK_URL=https://hooks.slack.com/services/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

  case "$2" in
    INFO)
      SLACK_ICON=':slack:'
      ;;
    WARNING)
      SLACK_ICON=':warning:'
      ;;
    ERROR)
      SLACK_ICON=':bangbang:'
      ;;
    SUCCESS)
      SLACK_ICON=':white_check_mark:'
      ;;
    *)
      SLACK_ICON=':slack:'
      ;;
  esac

  curl -X POST --data "payload={\"text\": \"${SLACK_ICON} $3 ${SLACK_MESSAGE}\"}" ${SLACK_URL}
}

function icmp_check () 
{
  flag=""
  COUNT=2
  for myHost in $HOSTS
  do
    count=$(ping -c $COUNT "$myHost" | grep 'received' | awk -F',' '{ print $2 }' | awk '{ print $1 }')
    if [ $count -eq 0 ]; then
      # 100% failed
      echo "$myHost is down" | mail -s "OpenStack New Cluster's Hosts Availability" $MAIL_GROUP 
      post_to_slack "$myHost is down (ping failed) at $(date)" "ERROR" "Ping Check"
    else
      flag+=$myHost"  "
    fi
  done
  if [ flag != "" ];then
    post_to_slack "New Cluster Hosts - $flag up (ping success) at $(date)" "SUCCESS" "Ping Check"
  fi
}

function service_check () 
{
  for myHost in $HOSTS
  do
    status=$(ssh -q -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no root@"$myHost" "openstack-service status"|awk -F"=" '{print $3=substr($3, 0,length($3)-12) " " $4}'|grep -v "active")
    if [ ! -z $status ]; then
      echo "$status is down at $myHost" | mail -s "OpenStack New Cluster's Services' Availability at Host-Level" $MAIL_GROUP
      post_to_slack "New Cluster Openstack Services down at $(date) for Host $myHost -  $status" "ERROR" "Systemctl check"
    fi
  if [  -z "$status" ];then
    post_to_slack "New Cluster Openstack Services up at $(date) for Host $myHost " "SUCCESS" "Systemctl check"
  fi
  done
}



function galera_check () 
{
    galera_cluster_size=$(ssh -q -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no root@$mysql_host "mysql -u root -p6#RvFs1Myq -e \"SHOW GLOBAL STATUS LIKE 'wsrep_cluster%';\""|grep wsrep_cluster_weight|awk -F" " '{print $2}')
    if [ $galera_cluster_size -lt 3 ]; then
      echo "New Cluster Galera is unhealthy with galera cluster size=$galera_cluster_size. Less than desiered limit of 3." | mail -s "OpenStack New Cluster's Services' Availability :: Galera DB Cluster" $MAIL_GROUP
      post_to_slack "New Cluster Galera is unhealthy with galera cluster size=$galera_cluster_size. Less than desiered limit of 3" "ERROR" "Mysql check"
    else
      post_to_slack "New Cluster Galera is healthy with galera cluster size=$galera_cluster_size" "SUCCESS" "Mysql check"
    fi
}

function peripheral_services_check()
{
  keepalived_status=()
  for myHost in $PERIPHERAL_SERVICE_HOSTS
  do
    keepalived_status+=($(ssh -q -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no root@$myHost systemctl is-active keepalived))
    
    httpd_status=$(ssh -q -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no root@$myHost systemctl is-active httpd)
    if [ $httpd_status != "active" ];then
      echo "New cluster HTTPD service is down at $(date) for Host $myHost "|mail -s "OpenStack New Cluster's HTTPD Service Availability" $MAIL_GROUP 
      post_to_slack "New Cluster Httpd service is down at $(date) for Host $myHost" "ERROR" "Systemctl check"
    else
      post_to_slack "New Cluster Httpd service is up at $(date) for Host $myHost" "SUCCESS" "Systemctl check"
    fi

    haproxy_status=$(ssh -q -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no root@$myHost systemctl is-active haproxy)
    if [ $haproxy_status != "active" ];then
      echo "New cluster HAPROXY service is down at $(date) for Host $myHost "|mail -s "OpenStack New Cluster's HAPROXY Service Availability" $MAIL_GROUP 
      post_to_slack "New Cluster HAProxy service is down at $(date) for Host $myHost" "ERROR" "Systemctl check"
    else
      post_to_slack "New Cluster HAProxy service is up at $(date) for Host $myHost" "SUCCESS" "Systemctl check"
    fi

    rabbitmq_status=$(ssh -q -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no root@$myHost systemctl is-active rabbitmq-server)
    if [ $rabbitmq_status != "active" ];then
      echo "New cluster RABBITMQ service is down at $(date) for Host $myHost "|mail -s "OpenStack New Cluster's RABBITMQ Service Availability" $MAIL_GROUP 
      post_to_slack "New Cluster RabbitMQ service is down at $(date) for Host $myHost" "ERROR" "Systemctl check"
    else
      post_to_slack "New Cluster RabbitMQ service is up at $(date) for Host $myHost" "SUCCESS" "Systemctl check"
    fi
  done 

  up_index_keepalived=($(printf '%s\n' "${keepalived_status[@]}"|grep -n active|cut -f1 -d:))
  if [ ${#up_index_keepalived[@]} -eq 0 ];then
    echo "New Cluster Openstack Keepalived service down on all HOSTS. Must be running on atleast 1 node. $(date)"| mail -s "OpenStack New Cluster's KEEPALIVED Service Availability" $MAIL_GROUP
    post_to_slack "New Cluster Keepalived service is down on all nodes. Must be up on atleast one." "ERROR" "Systemctl check"
  else
    post_to_slack "New Cluster Keepalived service is up at $(date)" "SUCCESS" "Systemctl check"
  fi
}

function rabbit_cluster_health_check()
{
  cluster_health_status=$(ssh -q -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no root@$controller rabbitmqctl cluster_status)
  partition_status=$(echo "$cluster_health_status"|grep partitions|awk -F, '{print $2=substr($2,0,length($2)-1)}') 
  if [ $partition_status != "[]" ];then
    echo "New Cluster RabbitMQ App is broken into partitions."|mail -s "Openstack New Cluster's RabbitMQ has partition" $MAIL_GROUP
    post_to_slack "New Cluster RabbitMQ App is broken into partitions" "ERROR" "Rabbitmqctl check"
  else
    post_to_slack  "New Cluster RabbitMQ App - No partitions" "SUCCESS" "Rabbitmqctl check"
  fi

  cluster_stat=$(echo "$cluster_health_status"|grep -B 50 cluster_name|grep -v "cluster_name\|Cluster")
  lines=$(echo "cluster_stat"|wc -l)
  total_nodes_line_count=$(echo "$lines/2"|bc)
  total_nodes=$(echo "$cluster_stat"|head -n $total_nodes_line_count|grep -o rabbit|wc -l)
  running_nodes=$(echo "$cluster_stat"|tail -n $total_nodes_line_count|grep -o rabbit|wc -l) 
  
  if [ ${#total_nodes} -ne ${#running_nodes} ];then
    echo "New Cluster RabbitMQ App - rabbitmq app is down. Total nodes not equal to running nodes."|mail -s "Openstack New Cluster's RabbitMQ app down" $MAIL_GROUP
    post_to_slack "New Cluster RabbitMQ App - rabbitmq app is down. Total nodes not equal to running nodes." "ERROR" "Rabbitmqctl check"
  else
    post_to_slack "New Cluster RabbitMQ App - All apps running. Total nodes are equal to running nodes." "SUCCESS" "Rabbitmqctl check"
  fi
}

# Execution scope 
icmp_check
service_check
galera_check
peripheral_services_check
rabbit_cluster_health_check
