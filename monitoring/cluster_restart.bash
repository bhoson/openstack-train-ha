#!/bin/bash
# Author: Snehadeep B.


MAIL='user1@email.com,user2@email.com'
openstack_services_hosts=(openstack06 openstack07 openstack08)
controller_hosts=(openstack06 openstack07 openstack08)
compute_hosts=(openstack-compute01)

function restart_compute_services()
{
  for host in "${compute_hosts[@]}"
  do
    ssh $host bash /script/restart_compute_services.bash restart
  done
}


function stop_openstack_services()
{
  for host in "${openstack_services_hosts[@]}"  
  do
    ssh $host openstack-service stop
  done
}

function stop_rabbitmq()
{
  # Reverse array
  controller_hosts_in_reverse=($(printf '%s\n' "${controller_hosts[@]}" |tac))
  for host in "${controller_hosts_in_reverse[@]}"
  do
    ssh $host systemctl stop rabbitmq-server
  done
}


function stop_httpd()
{
  for host in "${controller_hosts[@]}"
  do
    ssh $host systemctl stop httpd
  done
}


function stop_memcached()
{
  for host in "${controller_hosts[@]}"
  do
    ssh $host systemctl stop memcached
  done
}


function stop_haproxy()
{
  for host in "${controller_hosts[@]}"
  do
    ssh $host systemctl stop haproxy
  done
}


function start_memcached()
{
  for host in "${controller_hosts[@]}"
  do
    ssh $host systemctl start memcached
  done
}


function start_httpd()
{
  for host in "${controller_hosts[@]}"
  do
    ssh $host systemctl start httpd
  done
}


function start_rabbitmq()
{
  for host in "${controller_hosts[@]}"
  do
    ssh $host systemctl start rabbitmq-server
  done
}


function start_openstack_services()
{
  for host in "${openstack_services_hosts[@]}"  
  do
    ssh $host openstack-service start 
  done
}


function start_haproxy()
{
  for host in "${controller_hosts[@]}"
  do
    ssh $host systemctl start haproxy
  done
}


function get_status()
{
  for host in "${controller_hosts[@]}"
  do
    echo -e "\n\n For $host: \n" >> /tmp/cluster_restart_status_report
    echo -e "\n Openstack services \n" >> /tmp/cluster_restart_status_report
    ssh $host openstack-service status >> /tmp/cluster_restart_status_report
    echo -e "\n HAProxy \n" >> /tmp/cluster_restart_status_report
    ssh $host systemctl status haproxy | grep Active >> /tmp/cluster_restart_status_report
    echo -e "\n HTTPD \n" >> /tmp/cluster_restart_status_report
    ssh $host systemctl status httpd | grep Active >> /tmp/cluster_restart_status_report
    echo -e "\n Memcached \n" >> /tmp/cluster_restart_status_report
    ssh $host systemctl status memcached | grep Active >> /tmp/cluster_restart_status_report
    echo -e "\n RabbitMQ server \n" >> /tmp/cluster_restart_status_report
    ssh $host systemctl status rabbitmq-server | grep Active >> /tmp/cluster_restart_status_report
  done

  for host in "${compute_hosts[@]}"
  do
    echo -e "\n\n For Compute services on host - $host \n" >> /tmp/cluster_restart_status_report
    echo -e "\n neutron-linuxbridge-agent.service neutron-dhcp-agent.service neutron-metadata-agent.service libvirtd.service openstack-nova-compute.service " >> /tmp/cluster_restart_status_report
    ssh $host bash /script/restart_compute_services.bash status|grep Active >> /tmp/cluster_restart_status_report
  done
}


# Execution
> /tmp/cluster_restart_status_report 
stop_haproxy
stop_openstack_services
stop_rabbitmq
stop_httpd
stop_memcached
get_status
cat /tmp/cluster_restart_status_report | mail -s "Weekly cloud services maintenance [New cluster] - Stopping of services" $MAIL
sleep 10
> /tmp/cluster_restart_status_report 
start_memcached
start_httpd
start_rabbitmq
start_haproxy
start_openstack_services
# restart services on compute node
restart_compute_services
get_status
cat /tmp/cluster_restart_status_report | mail -s "Weekly cloud services maintenance [New cluster] - Starting of services" $MAIL

