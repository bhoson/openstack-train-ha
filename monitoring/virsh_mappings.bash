#!/bin/bash
DBUSER='root'
DBPASS='6#RvFs1Myq'
vm_visrh_doms=($(virsh list|sed  -n '1,2!p'|awk '{print $2}'))
MAIL='tapash.raha@seamless.se,anirban.das@seamless.se,snehadeep.bhowmick@seamless.se'
LOG='/tmp/virshlogger.log'
CONTROLLER=$(hostname)

touch $LOG
> $LOG
for i in "${vm_visrh_doms[@]}"
do
  uuid=$(virsh domuuid $i)
  printf "\n===========================================================================\n" >> $LOG
  printf "\n VM (virsh ID) -  $i\n" >> $LOG
  printf "\n== VM Properties =============================================================\n"  >> $LOG
  command="select vm_state as Status,launched_on,hostname as Name,uuid as ID from nova.instances where uuid='$uuid' and vm_state='active';"
  vm_details=$(mysql -t -u$DBUSER -p$DBPASS -e "$command")
  command="select ip_address from neutron.ipallocations where port_id in (select id from neutron.ports where device_id='$uuid')"
  ip_address=$(mysql -t -u$DBUSER -p$DBPASS -e"$command")
  echo "$vm_details"  >> $LOG
  printf "\n"
  echo "$ip_address"  >> $LOG
  printf "\n"
  command="select name as Project from keystone.project where id in (select project_id from nova.instances where uuid='$uuid' and vm_state='active');"
  project_name=$(mysql -t -u$DBUSER -p$DBPASS -e"$command")
  echo "$project_name" >> $LOG
  printf "\n"
  net_tap_device=$(virsh domiflist $i)
  echo "$net_tap_device" >> $LOG
  printf "\n============================================================================\n\n\n"  >> $LOG
done


cat $LOG | mail -s "Virsh listings for $CONTROLLER" $MAIL
