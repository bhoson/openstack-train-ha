#!/bin/bash

CMD=$1

systemctl $CMD neutron-linuxbridge-agent.service neutron-dhcp-agent.service neutron-metadata-agent.service
systemctl $CMD libvirtd.service
systemctl $CMD openstack-nova-compute.service
