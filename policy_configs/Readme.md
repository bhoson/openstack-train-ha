### All policy files are to be placed as the following:

	/etc/keystone/policy.json
	/etc/cinder/policy.json
	/etc/glance/policy.json


## For enabling other users/projects/tenents to create cinder volumes:

1) Change quota from dashboard.
2) Apply per-volume-gigabytes quota from cli: 


		$ cinder quota-show 5d9cd1a8b0d6456ba9927ffba232303e
		+-----------------------+-------+
		| Property              | Value |
		+-----------------------+-------+
		| backup_gigabytes      | 1000  |
		| backups               | 10    |
		| gigabytes             | 200   |
		| gigabytes___DEFAULT__ | -1    |
		| gigabytes_multiattach | -1    |
		| groups                | 10    |
		| per_volume_gigabytes  | 0     | *
		| snapshots             | 0     |
		| snapshots___DEFAULT__ | -1    |
		| snapshots_multiattach | -1    |
		| volumes               | 1     |
		| volumes___DEFAULT__   | -1    |
		| volumes_multiattach   | -1    |
		+-----------------------+-------+
		
		$ cinder quota-update --per-volume-gigabytes 200 5d9cd1a8b0d6456ba9927ffba232303e
		+-----------------------+-------+
		| Property              | Value |
		+-----------------------+-------+
		| backup_gigabytes      | 1000  |
		| backups               | 10    |
		| gigabytes             | 200   |
		| gigabytes___DEFAULT__ | -1    |
		| gigabytes_multiattach | -1    |
		| groups                | 10    |
		| per_volume_gigabytes  | 200   | *
		| snapshots             | 0     |
		| snapshots___DEFAULT__ | -1    |
		| snapshots_multiattach | -1    |
		| volumes               | 1     |
		| volumes___DEFAULT__   | -1    |
		| volumes_multiattach   | -1    |
		+-----------------------+-------+




## For further reading about policy:

	https://docs.openstack.org/oslo.policy/latest/admin/policy-json-file.html
	https://docs.openstack.org/cinder/queens/configuration/block-storage/policy.html
	https://docs.openstack.org/cinder/queens/sample_policy.html
	https://docs.openstack.org/cinder/queens/configuration/block-storage/samples/policy.json.html



